package org.ets.clep;

import software.amazon.awscdk.core.App;

public class JavaAwsCdkApp {
    public static void main(final String[] args) {
        App app = new App();

        new JavaAwsCdkStack(app, "JavaAwsCdkStack");

        app.synth();
    }
}
